// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from "axios";
import qs from "qs";
import HTTP_STATUS from "@src/constants/httpStatus";
import ENV from "@src/constants/env";
import { getAccessToken, getRefreshToken } from "@src/utils/token";
import { removeCookie } from "@src/app/cookie";

const NODE_ENV = ENV.NODE_ENV

let isRefreshing = false;
// eslint-disable-next-line @typescript-eslint/ban-types
let failedQueue: { resolve: Function; reject: Function }[] = [];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const processQueue = (error: any, token: string | null = null) => {
  failedQueue.forEach(prom => {
    if (error) {
      prom.reject(error);
    } else {
      prom.resolve(token);
    }
  });

  failedQueue = [];
};

const baseURLApp = ENV.API_URL;
const apiClient = axios.create({
  baseURL: baseURLApp,
  headers: {
    "content-type": "application/json",
  },

  paramsSerializer: (params: unknown) => {
    return qs.stringify(params);
  },
});

apiClient.interceptors.request.use(
  async (config: AxiosRequestConfig) => {
    const accessToken = getAccessToken();

    if (accessToken && config.headers) {
      config.headers.Authorization = `Bearer ${accessToken}`;
    }
    return config;
  },
  (error : AxiosError) => {
    return Promise.reject(error);
  },
);

apiClient.interceptors.response.use(
  (response: AxiosResponse) => {
    return response;
  },
  (err: AxiosError) => {
    const originalRequest = err.config ;
    const data = err?.response?.data;
    if (data?.code === HTTP_STATUS.EXPIRED_ACCESS_TOKEN && !originalRequest?._retry) {
      if (isRefreshing) {
        return new Promise<void>((resolve, reject) => {
          failedQueue.push({ resolve, reject });
        })
          .then(() => {
            const token = getAccessToken();
            if (token) {
              originalRequest.headers["Authorization"] = "Bearer " + token;
              return axios(originalRequest);
            } else {
              throw new Error("Access token not found.");
            }
          })
          .catch(err => {
            return Promise.reject(err);
          });
      }

      originalRequest._retry = true;
      isRefreshing = true;

      return new Promise<void>((resolve, reject) => {
        const refreshToken = getRefreshToken();
        axios
          .post("/api/v1/common/auth/refresh-token", {
            refreshToken: refreshToken,
          })
          .then(({ data }) => {
            axios.defaults.headers.common["Authorization"] = "Bearer " + data?.accessToken;
            originalRequest.headers["Authorization"] = "Bearer " + data?.accessToken;
            processQueue(null, data?.accessToken);
            resolve(axios(originalRequest));
          })
          .catch(err => {
            processQueue(err, null);
            console.error("Expired Token", err);
            reject(err);
          })
          .then(() => {
            isRefreshing = false;
          });
      });
    }

    return Promise.reject(err);
  },
);

apiClient.interceptors.response.use(
  (response: AxiosResponse) => {
    if (response && response.data) {
      NODE_ENV !== "production" &&
        console.debug(
          response.status,
          response.config.method?.toUpperCase(),
          (response.config.baseURL || "") + response.config.url,
          response.data,
        );
      return response.data;
    }
    return response;
  },
  (error: AxiosError) => {
    const response = error?.response?.data;
    const statusCode = error.response?.status;
    if (statusCode === HTTP_STATUS.UNAUTHORIZED) {
      removeCookie();
    }
    if (statusCode === HTTP_STATUS.INTERNAL_SERVER_ERROR) {
      if (response?.code === 0) {
        // clearStorage();
      }
    }
    return Promise.reject(error);
  },
);

export default apiClient;
