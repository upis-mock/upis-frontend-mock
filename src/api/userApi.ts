import apiClient from "./apiClient";

const userApi = {
  getInfo() {
    const url = "/users/get-info";
    return apiClient.get(url);
  },
};
export default userApi;
