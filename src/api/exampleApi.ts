import objectToFormData from "@src/utils/objectToFormData";
import apiClient from "./apiClient";

const exampleApi = {
  getListExample<T>(options?: { headers?: Record<string, string>; params?: any }) {
    const url = "/api/v1/example";
    return apiClient.get<T>(url, options);
  },

  getExampleById<T>(id: string, options?: { headers?: Record<string, string>; params?: any }) {
    const url = `/api/v1/example/${id}`;
    return apiClient.get<T>(url, options);
  },

  createExample<T>(
    data: any,
    options?: { headers?: Record<string, string>; params?: any }
  ) {
    const url = "/api/v1/example";
    return apiClient.post<T>(url, data, options);
  },

  createExampleBySomeThing<T>(
    something: any,
    data: any,
    options?: { headers?: Record<string, string>; params?: any }
  ) {
    const { id, commentId, postId } = something;
    const url = `/api/v1/example/${postId}/${commentId}/${id}`;
    return apiClient.post<T>(url, data, options);
  },

  putExampleById<T>(
    id: string,
    data: any,
    options?: { headers?: Record<string, string>; params?: any }
  ) {
    const url = `/api/v1/example/${id}`;
    return apiClient.put<T>(url, data, options);
  },

  putExampleBySomething<T>(
    something: any,
    data: any,
    options?: { headers?: Record<string, string>; params?: any }
  ) {
    const { id, commentId, postId } = something;
    const url = `/api/v1/example/${postId}/${commentId}/${id}`;
    return apiClient.put<T>(url, data, options);
  },

  deleteExampleById<T>(
    id: string,
    options?: { headers?: Record<string, string>; params?: any }
  ) {
    const url = `/api/v1/example/${id}`;
    return apiClient.delete<T>(url, options);
  },

  deleteExampleBySomething<T>(
    something: any,
    options?: { headers?: Record<string, string>; params?: any }
  ) {
    const { id, commentId, postId } = something;
    const url = `/api/v1/example/${postId}/${commentId}/${id}`;
    return apiClient.delete<T>(url, options);
  },

  patchExampleById<T>(
    id: string,
    data: any,
    options?: { headers?: Record<string, string>; params?: any }
  ) {
    const url = `/api/v1/example/${id}`;
    return apiClient.patch<T>(url, data, options);
  },

  patchExampleBySomething<T>(
    something: any,
    data: any,
    options?: { headers?: Record<string, string>; params?: any }
  ) {
    const { id, commentId, postId } = something;
    const url = `/api/v1/example/${postId}/${commentId}/${id}`;
    return apiClient.patch<T>(url, data, options);
  },

  // form data
  createExampleWithFormData<T>(
    data: any,
    options?: { headers?: Record<string, string>; params?: any }
  ) {
    const url = "/api/v1/example";
    // Tạo headers nếu options không được cung cấp hoặc không có headers
    const headers = options?.headers || {};
    // Đặt Content-Type là multipart/form-data để gửi formData
    headers['Content-Type'] = 'multipart/form-data';
    // transfer form data
    const formData = objectToFormData(data);
    return apiClient.post<T>(url, formData, { ...options, headers });
  },
};

export default exampleApi;
