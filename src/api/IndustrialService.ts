import objectToFormData from "@src/utils/objectToFormData";
import apiClient from "./apiClient";

const IndustrialService = {

    getList<T>(options?: { headers?: Record<string, string>; params?: any }) {
        const url = "/api/industrial";
        return apiClient.get<T>(url, options);
    },
    
    getDetail<T>(id: string, options?: { headers?: Record<string, string>; params?: any }) {
    const url = `/api/industrial/${id}`;
    return apiClient.get<T>(url, options);
    },

    // create<T>(
    //     data: any,
    //     options?: { headers?: Record<string, string>; params?: any }
    //   ) {
    //     const url = "/api/company";
    //     // Tạo headers nếu options không được cung cấp hoặc không có headers
    //     const headers = options?.headers || {};
    //     // Đặt Content-Type là multipart/form-data để gửi formData
    //     headers['Content-Type'] = 'multipart/form-data';
    //     // transfer form data
    //     const formData = objectToFormData(data);
    //     return apiClient.post<T>(url, formData, { ...options, headers });
    // },

    create<T>(
        data: any,
        options?: { headers?: Record<string, string>; params?: any }
    ) {
        const url = "/api/industrial";
        return apiClient.post<T>(url, data, options);
    },

    update<T>(
        id: string,
        data: any,
        options?: { headers?: Record<string, string>; params?: any }
    ) {
        const url = `/api/industrial/${id}`;
        return apiClient.put<T>(url, data, options);
    },
}

export default IndustrialService;