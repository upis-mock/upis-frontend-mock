import ICompany from "@src/interfaces/ICompany";
import apiClient from "./apiClient";

const CompanyService = {

    getList(options?: { headers?: Record<string, string>; params?: any }) {
        const url = "/api/company";
        return apiClient.get<ICompany[]>(url, options);
    },
    
    getDetail(id: string, options?: { headers?: Record<string, string>; params?: any }) {
    const url = `/api/company/${id}`;
    return apiClient.get(url, options);
    },

    // create<T>(
    //     data: any,
    //     options?: { headers?: Record<string, string>; params?: any }
    //   ) {
    //     const url = "/api/company";
    //     // Tạo headers nếu options không được cung cấp hoặc không có headers
    //     const headers = options?.headers || {};
    //     // Đặt Content-Type là multipart/form-data để gửi formData
    //     headers['Content-Type'] = 'multipart/form-data';
    //     // transfer form data
    //     const formData = objectToFormData(data);
    //     return apiClient.post<T>(url, formData, { ...options, headers });
    // },

    create(
        data: any,
        options?: { headers?: Record<string, string>; params?: any }
    ) {
        const url = "/api/company";
        return apiClient.post(url, data, options);
    },

    update(
        id: string,
        data: any,
        options?: { headers?: Record<string, string>; params?: any }
    ) {
        const url = `/api/company/${id}`;
        return apiClient.put(url, data, options);
    },

    delete(
        id: string,
        options?: { headers?: Record<string, string>; params?: any }
      ) {
        const url = `/api/company/${id}`;
        return apiClient.delete(url, options);
      },
}

export default CompanyService;