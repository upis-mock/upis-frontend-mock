import ENV from "@src/constants/env";
import apiClient from "./apiClient";

const authApi = {
  login(params :string) {
    apiClient.defaults.baseURL = ENV.AUTH.AUTH_URL
    const url = `/token?${params}`;
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
    }

    console.log('url', url)
    return apiClient.post(url, { }, {
      headers,
      auth: {
        username: ENV.AUTH.CLIENT_ID,
        password: ENV.AUTH.VITE_CLIENT_SECRET
      }
    });
  },

  refreshToken(payload: any) {
    const url = "/api/v1/common/auth/refresh-token";
    return apiClient.post(url, payload);
  },

  deleteAccount(payload: any) {
    const url = "/api/v1/delete-account";
    return apiClient.post(url, payload);
  },
  
  logout(payload: any) {
    const url = "/api/v1/logout";
    return apiClient.post(url, payload);
  },
};
export default authApi;
