const colors = {
    // test
    test_red: "#FF0000",
    test_primary: '#4096ff',
    test_primary_light: '#0958d9',
    // end test
    white: "#ffffff",
    black: "#000000",
  };
  
  export default colors;