import './App.css';
import i18n from '@src/languages/i18n.ts';
import { I18nextProvider } from 'react-i18next';
import { appRoutes } from './routes/app.routes';
import { RouterProvider } from 'react-router-dom';
import AuthLayout from '@src/layouts/AuthLayout';
import ConfigLayout from '@src/layouts/ConfigLayout';
import { Provider } from 'jotai/react'
import {
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query'
import { optionsQueryClient } from '@src/react-query/configs/defaultOptions';
import { useHydrateAtoms } from 'jotai/react/utils'
import { queryClientAtom } from 'jotai-tanstack-query'
import { jotaiStore } from './jotai/store';

const queryClient = new QueryClient(optionsQueryClient)

const HydrateAtoms = ({ children }: any) => {
  useHydrateAtoms([[queryClientAtom, queryClient]] as any)
  return children
}

function App() {
  return (
    <I18nextProvider i18n={i18n} defaultNS={'translation'}>
      <QueryClientProvider client={queryClient}>
        <Provider store={jotaiStore} >
          <HydrateAtoms>

            <ConfigLayout>
              <AuthLayout>
                <RouterProvider router={appRoutes} />
              </AuthLayout>
            </ConfigLayout>

          </HydrateAtoms>
        </Provider>
      </QueryClientProvider>
    </I18nextProvider>
  );
}

export default App;
