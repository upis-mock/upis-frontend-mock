import { useLoaderData } from 'react-router-dom';

export default function HomePage() {
  const loaderData = useLoaderData();
  console.log('====================================');
  console.table(loaderData);
  console.log('====================================');

  const renderContentFake = () => { 
    return [new Array(50).fill(null).map((_d, index)=> <div key={index} className='py-10'>line {index}</div>)]
  }
  return (
  <div className='bg-[#0958d9] text-center text-white'>
  </div>
  )
}
