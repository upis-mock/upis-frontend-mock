import TestLayout from '@src/layouts/TestLayout';
import TestPage from '@src/modules/test/pages/TestPage';
import AntdPage from './pages/AntdPage';

export const testRoutes = [
  {
    path: "test",
    element: <TestLayout/>,
    children: [
      {
          path: "index",
          element: <TestPage />,
      },
      {
        path: "antd",
        element: <AntdPage />,
      },
    ],
  },
]