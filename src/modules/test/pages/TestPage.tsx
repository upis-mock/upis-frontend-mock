import { DatePicker } from 'antd';
import useCustomTranslation from '@src/languages/useCustomTranslation';
import { useTranslation } from 'react-i18next';
import { login } from '@src/services/auth.service';

export default function TestPage() {
  const { t } = useTranslation(); 
  const {changeLocale} = useCustomTranslation();
  return (
    <div>TestPage
      <DatePicker.RangePicker />
      <label>{t('name.label')}</label>
      <label>{t('age.label')}</label>
      <label>{t('home.label')}</label>

      <div className='flex flex-row gap-5'>
      <div onClick={()=>changeLocale ('vi')}>VI</div>
      <div onClick={()=>changeLocale ('en')}>en</div>
      </div>
      <div className="my-20 p-5 bg-slate-50 cursor-pointer" onClick={login}>
      Đăng nhập
      </div>
    </div>
  )
}
