import { Button } from 'antd';
import { useRef, useState } from 'react';

export default function TestUseRef() {
  let ref = useRef(0);

  function handleClick() {
    ref.current = ref.current + 1;
    alert('You clicked ' + ref.current + ' times!');
  }

  const [count, setCount] = useState(0);

  return (
   <div className='text-center p-[50px]'>
     <Button onClick={handleClick}>
      Click me!
    </Button>
    

    <div className='my-[30px]'>Ref current : {ref.current}</div>

    <Button onClick={()=> setCount(count + 1)}>
      Render component
    </Button>
   </div>
  );
}
