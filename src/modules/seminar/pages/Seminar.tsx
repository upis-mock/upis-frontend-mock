

type SeminarItemProps = { 
  title: string;
  name: string;
}

function ChildComponent(props : SeminarItemProps) {
  const { title, name } = props;

  return (
    <div className="text-blue-600">
        <div>
          {title}
        </div>
        <p>{name}</p>
    </div>
  );
}


export default function ParentComponent() {

  const title= 'Dữ liệu từ Parent component';

  return (
    <div className="text-center p-[20px] flex flex-col gap-[20px]">
      <h1 className=" text-orange-600"> Page seminar </h1>

      <ChildComponent title={title} name="Item 1"/>

    </div>
  )
}
