import { Button, Flex } from 'antd';
import React, { useState } from 'react'

export default function TestUseStateComponent() {
    let indexItem = 0;
    const [count, setCount] = useState(0);

    const incrementRegularVariable = () =>{ 
        indexItem = indexItem + 1;
        // do indexItem không phải là 1 state nên component sẽ không render
    }
    const incrementCount = () =>{
        setCount(count + 1)
        // --> Render component
    }

    const bugIncrementCount_3 = () =>{
        // logic 1...
        setCount(count + 1); // setCount( 0 => 1 )
        // logic 2...
        setCount(count + 1); // setCount( 0 => 1 )
        // logic 3...
        setCount(count + 1); // setCount( 0 => 1 )
        // --> Render component
    }

    const incrementCount_3 = () =>{
        setCount(pre => pre + 1); // setCount( 0 => 1 )
        setCount(pre => pre + 1); // setCount( 1 => 2 )
        setCount(pre => pre + 1); // setCount( 2 => 3 )
        // --> Render component
    }
    return (
        <div className='text-center p-[50px] flex flex-col gap-[10px]'>
            <h2 className='text-[24px] text-red-600'>
                Regular variable : {indexItem}
            </h2>

            <h2 className='mb-10 text-[24px] text-blue-600'>
                {`count: ${count}`}
            </h2>

            <Flex gap="large" wrap="wrap" justify='center'>
                <Button onClick={incrementRegularVariable} type="primary">Regular variable + 1 </Button>

                <Button onClick={incrementCount} type="primary">Count + 1 </Button>

                <Button onClick={bugIncrementCount_3} type="primary">Bug count + 3 </Button> 

                <Button onClick={incrementCount_3} type="primary">Count + 3 </Button>

                <Button onClick={()=> setCount(0)} type="primary">Reset count </Button>
            </Flex>
            
        </div>
    )
}
