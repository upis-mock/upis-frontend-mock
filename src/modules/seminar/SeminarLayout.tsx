import React from 'react'
import { Outlet } from 'react-router-dom'

export default function SeminarLayout() {
  return (
  <div>
    <div className='p-[20px] bg-emerald-500 text-center text-white'>
        Header Seminar
    </div>
     <Outlet/>
  </div>
  )
}
