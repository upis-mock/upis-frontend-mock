import TestUseRef from './pages/Ref';
import Seminar from './pages/Seminar';
import State from './pages/State';
import SeminarLayout from './SeminarLayout';

export const seminarRoutes = [
  {
    path: "seminar",
    element: <SeminarLayout/>,
    children: [
      {
          path: "index",
          element: <Seminar />,
      },
      {
        path: "state",
        element: <State />,
      },
      {
        path: "ref",
        element: <TestUseRef />,
      },
    ],
  },
]