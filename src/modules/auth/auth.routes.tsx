import LoginPage from './pages/LoginPage';
import { redirectIfToken } from '@src/routes/loader';

export const authRoutes = [
  {
    path: "auth",
    // element: <LoginPage/>,
    children: [
      {
          path: "login",
          element: <LoginPage />,
          loader: redirectIfToken
      },
    ],
  },
]