import { ACCESS_TOKEN } from "@src/constants/app";
import Cookies from 'js-cookie';
import { login } from "@src/services/auth.service";

export default function LoginPage() {
 
  const addToken = () =>{
    Cookies.set(ACCESS_TOKEN, ACCESS_TOKEN);
    localStorage.setItem(ACCESS_TOKEN, ACCESS_TOKEN);
  }
  return (
    <div className="bg-red ">

    <h1>
    Đây là trang đăng nhập
    </h1>
    <div>ENV : {import.meta.env.DEV}</div>
    
    <div className="mt-20">
    <div className="my-20 p-5 bg-slate-50 cursor-pointer" onClick={login}>
     Đăng nhập
    </div>

    <div className="my-20 p-5 bg-slate-50 cursor-pointer" onClick={addToken}>
     Đăng xuất
    </div>
    </div>

    </div>
  )
}