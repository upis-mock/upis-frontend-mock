import ErrorBoundary from './pages/ErrorBoundary';

export const baseRoutes =[
    {
      path: "/500",
      element: <ErrorBoundary />,
    },
  ];
  