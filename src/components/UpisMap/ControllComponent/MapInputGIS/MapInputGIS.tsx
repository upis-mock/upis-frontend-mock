import { useState } from 'react';
import './MapInputGIS.scss';
import { Alert, Button, Modal, Space } from 'antd';

type MapInputGISProps = {
    
}

export default function MapInputGIS(props: MapInputGISProps) {

    const [actived, setActived] = useState(false);
    const [openModal, setIsOpenGis] = useState(false);

    const showModal = () => {
        setIsOpenGis(!openModal);
        setActived(!actived);
    }

    return (
        <>
            <div className="map-input-gis" key={123}>
                <div onClick={() => showModal()} className='items'>
                    <div className={'i-box' + (actived ? ' actived' : '')}>
                        <img src='/insert-gis.svg' />
                    </div>
                </div>
                <Modal title="Nhập thông tin" open={openModal} footer={null} width={300} onCancel={showModal}>
                    <Alert message="Hệ thống sử dụng hệ tọa độ EPSG:4326, Bạn cần sử dụng chính xác hệ tọa độ để tương thích với hệ thống." type="warning" showIcon />
                    <div className="upload-wrapper">
                        <div className="upload-file">
                            <img src="/upload-gis.svg" alt="upload-gis" />
                            <div className="upload-file-desc">
                                Nhấn hoặc kéo thả để tải lên dữ liệu
                            </div>
                            <div className="upload-file-note">
                                Chỉ hỗ trợ tệp dữ liệu GIS
                            </div>
                        </div>
                        {/* <input
                            ref={fileInputRef}
                            type="file"
                            accept=".json"
                            style={{ display: 'none' }}
                            onChange={handleFileInputChange}
                        /> */}
                    </div>
                    <div className="bottom-group-btn">
                        <Space style={{ width: "100%", justifyContent: "end" }}>
                            <Button htmlType="button" onClick={showModal}>
                                Hủy
                            </Button>
                            <Button type="primary" htmlType="button" >
                                Tạo mới
                            </Button>
                        </Space>
                    </div>
                </Modal>
            </div>
        </>
    )
}