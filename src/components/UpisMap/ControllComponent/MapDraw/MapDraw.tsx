import { useState } from 'react';
import './MapDraw.scss';

type MapDrawProps = {
    onDraw?: any;
}
const POLYGON = "Polygon";
const POINT = "Point";

export default function MapDraw(props: MapDrawProps) {

    const [type, setType] = useState<string>();

    const draw = (t: string) => {
        t = type === t ? '' : t;
        setType(t);
        props.onDraw(t);
    }

    return (
        <>
            <div className="map-draw" key={123}>
                <div onClick={() => draw(POLYGON)} className='items'>
                    <div className={'i-box' + (type === POLYGON ? ' actived' : '')}>
                        <img src='/icon-draw-polygon.svg' />
                    </div>
                </div>
                <div onClick={() => draw(POINT)} className='items'>
                    <div className={'i-box' + (type === POINT ? ' actived' : '')}>
                        <img src='/icon-draw-point.svg' />
                    </div>
                </div>
            </div>
        </>
    )
}