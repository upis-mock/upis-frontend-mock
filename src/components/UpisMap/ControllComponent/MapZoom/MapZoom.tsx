import './MapZom.scss';

type MapZoomProps = {
    onZoom?: any;
}
export default function MapZoom(props: MapZoomProps) {

    return (
        <div className="map-zoom">
            <div onClick={() => props.onZoom(true)}>
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.67 1.96484H9.33067C9.21162 1.96484 9.1521 2.02437 9.1521 2.14342V9.15234H2.50084C2.38179 9.15234 2.32227 9.21187 2.32227 9.33092V10.6702C2.32227 10.7893 2.38179 10.8488 2.50084 10.8488H9.1521V17.8577C9.1521 17.9767 9.21162 18.0363 9.33067 18.0363H10.67C10.789 18.0363 10.8485 17.9767 10.8485 17.8577V10.8488H17.5008C17.6199 10.8488 17.6794 10.7893 17.6794 10.6702V9.33092C17.6794 9.21187 17.6199 9.15234 17.5008 9.15234H10.8485V2.14342C10.8485 2.02437 10.789 1.96484 10.67 1.96484Z" fill="black" fillOpacity="0.88" />
                </svg>
            </div>
            <div onClick={() => props.onZoom(false)}>
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18.0352 9.15234H1.96373C1.86551 9.15234 1.78516 9.2327 1.78516 9.33092V10.6702C1.78516 10.7684 1.86551 10.8488 1.96373 10.8488H18.0352C18.1334 10.8488 18.2137 10.7684 18.2137 10.6702V9.33092C18.2137 9.2327 18.1334 9.15234 18.0352 9.15234Z" fill="black" fillOpacity="0.88" />
                </svg>
            </div>
        </div>
    )
}