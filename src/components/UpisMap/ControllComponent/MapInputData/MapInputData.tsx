import { useState } from 'react';
import './MapInputData.scss';
import { Alert, Button, Form, InputNumber, Modal, Space } from 'antd';

type MapInputDataProps = {

}

type PointType = {
    longitude?: number,
    latitude?: number,
}

export default function MapInputData(props: MapInputDataProps) {

    const [actived, setActived] = useState(false);
    const [openModal, setOpenModal] = useState(false);

    const showModal = () => {
        setOpenModal(!openModal);
        setActived(!actived);
    }


    return (
        <>
            <div className="map-input-data" key={123}>
                <div onClick={() => showModal()} className='items'>
                    <div className={'i-box' + (actived ? ' actived' : '')}>
                        <img src='/coordinates.svg' />
                    </div>
                </div>
                <Modal title="Nhập thông tin" open={openModal} footer={null} width={300} onCancel={showModal}>
                    <Alert message="Hệ thống sử dụng hệ tọa độ EPSG:4326, Bạn cần sử dụng chính xác hệ tọa độ để tương thích với hệ thống." type="warning" showIcon />
                    <Form
                        name="basic"
                        labelCol={{ span: 24 }}
                        wrapperCol={{ span: 24 }}
                        layout={"vertical"}
                        style={{ maxWidth: 300, marginTop: "20px" }}
                    // onFinish={(values) => ()}
                    >
                        <Form.Item<PointType>
                            label="Kinh độ:"
                            name="longitude"
                            rules={[{ required: true, message: 'Please input longitude!' }]}
                        >
                            <InputNumber placeholder="Nhập kinh độ..." style={{ width: "100%" }} />
                        </Form.Item>

                        <Form.Item<PointType>
                            label="Vĩ độ:"
                            name="latitude"
                            rules={[{ required: true, message: 'Please input longitude!' }]}
                        >
                            <InputNumber placeholder="Nhập vĩ độ..." style={{ width: "100%" }} />
                        </Form.Item>
                        <Form.Item >
                            <Space style={{ width: "100%", justifyContent: "end" }}>
                                <Button htmlType="button" onClick={showModal}>
                                    Hủy
                                </Button>
                                <Button type="primary" htmlType="submit">
                                    Tạo mới
                                </Button>
                            </Space>
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        </>
    )
}