import { Overlay } from 'ol';
import React, { useEffect } from 'react'

export default function DeleteFeatureOverlay({ map }: { map: any }) {
    const overlayRef = React.useRef(null);

    useEffect(() => {
        if (!overlayRef.current || !map) return;

        const overlay = new Overlay({
            element: overlayRef.current,
            positioning: 'bottom-center',
            offset: [0, -10],
        });

        map.addOverlay(overlay);

        return () => map.removeOverlay(overlay);
    }, [map])

    const handleDeleteFeature = () => {

    }

    return (
        <div className='map-overlay' ref={overlayRef}>
            <button onClick={handleDeleteFeature}>Delete Feature</button>
        </div>
    )
}
