import { MAP } from '@src/constants/mapConstanst';
import Map from 'ol/Map';
import { WKT } from 'ol/format';
import { Point, Polygon } from 'ol/geom';
import { Draw } from 'ol/interaction';
import VectorSource from 'ol/source/Vector';


let map: Map;
let source: VectorSource;

export function mapperData(m: Map, s: VectorSource) {
    map = m;
    source = s;
}

export function styleDraw(drawType?: any) {
    const styles = [];

    if (drawType === 'Polygon') {
        styles.push(MAP.POLYGON.STYLE_DEFAULT);
    } else if (drawType === 'Point') {
        styles.push(MAP.MARKER.STYLE_DEFAULT);
    }
    return styles;
}

export function addInteraction(drawType: any, onChangeFeature: (wkt: string) => void) {
    const draw = new Draw({
        source: source,
        type: drawType,
        style: function (feature) {
            return styleDraw(drawType);
        },
    });

    draw.on('drawstart', function () {
        if (drawType === 'Point') {
            source.clear();
        }

    });
    draw.on('drawend', function (e) {
        if (drawType === 'Point') {
            e.feature.setStyle(MAP.MARKER.STYLE_DEFAULT);
        }
        onChangeFeature(getAllFeaturesAsWKT(e.feature));
    });

    map?.addInteraction(draw);

    return draw;
}

export function getAllFeaturesAsWKT(feature: any): string {
    // const features = source.getFeatures();
    // const wktFormatter = new WKT();
    // return features.map(feature => wktFormatter.writeFeature(feature)).join(',');

    const wktFormatter = new WKT();
    return wktFormatter.writeFeature(feature)

}

export function clearDraw(draw: any) {
    map?.removeInteraction(draw);
};