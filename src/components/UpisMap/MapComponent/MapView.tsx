import { Map, View } from "ol";
import TileLayer from "ol/layer/Tile";
import VectorLayer from "ol/layer/Vector";
import { OSM } from "ol/source";
import VectorSource from "ol/source/Vector";
import { useEffect, useRef, useState } from "react";
import { mapperData } from "../MapService";
import "./MapView.scss";
import { WKT } from "ol/format";
import { MAP } from "@src/constants/mapConstanst";
import MapZoom from "../ControllComponent/MapZoom/MapZoom";
import { Button } from "antd";

// init maps features
const raster = new TileLayer({
    source: new OSM(),
});
const source = new VectorSource({ wrapX: false });
const vector = new VectorLayer({
    source: source,
});
const initView = {
    projection: 'EPSG:3857',
    center: [1388524.419, 2349898.577],
    zoom: 2,
};

type MapViewProps = {
    wkt?: string;
    onChangeFeature?: any;
    showPopup?: any;
}

export default function MapView(props: MapViewProps) {
    const mapElement = useRef<HTMLDivElement | null>(null);
    const [vectorMap, setVectorMap] = useState(vector);
    const [map, setMap] = useState<Map>();
    const initialized = useRef(false);
    const format = new WKT();

    useEffect(() => {
        console.log('wkt', props.wkt);
        
        if (!initialized.current) {
            initialized.current = true
            initMap(mapElement);
        
        }
    }, []);

    function initMap(mapElement: React.MutableRefObject<HTMLDivElement | null>): Map | undefined {
        const map = new Map({
            layers: [raster, vector],
            view: new View(initView),
            controls: [],
        });
        map.setTarget(mapElement.current as any);

        mapperData(map, source);
        if (props.wkt) {
            const feature = format.readFeature(props.wkt, {
                dataProjection: 'EPSG:3857',
                featureProjection: 'EPSG:3857',
                });
            const vector1 = new VectorLayer({
                source: new VectorSource({
                    features: [feature],
                }),
                style: MAP.MARKER.STYLE_DEFAULT
            });
            map?.addLayer(vector1);
        }
        setMap(map);
        return map;
    }

    const onZoom = (zo: boolean) => {
        if (map) {
            let view = map.getView();
            let zoom = view.getZoom();
            if (zoom) view.setZoom(zo ? zoom + 1 : zoom - 1);
        }
    }

    return (
        <div className='map-view-wrapper'>
            <div className='map-container' ref={mapElement}>
                <div className="actions">
                    <Button type="primary" onClick={() => props.showPopup(true)} >Sửa</Button>
                </div>
                <div className="functions">
                    <MapZoom onZoom={(zoom: boolean) => onZoom(zoom)} />
                </div>
            </div>
        </div>
    )
}