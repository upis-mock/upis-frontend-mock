import React, { useEffect, useRef, useState } from 'react'
import "./MapComponent.scss";
import { Map, View } from "ol";
import { fromLonLat, toLonLat } from 'ol/proj';
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";
import { defaults as defaultControls } from "ol/control/defaults";
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Interaction from 'ol/interaction/Interaction';
import { XYZ } from 'ol/source';
import { MAP, QUY_NHON_CENTER, ZOOM_DEFAULT } from '@src/constants/mapConstanst';
import MapZoom from '../ControllComponent/MapZoom/MapZoom';
import MapDraw from '../ControllComponent/MapDraw/MapDraw';
import MapInputData from '../ControllComponent/MapInputData/MapInputData';
import MapInputGIS from '../ControllComponent/MapInputGIS/MapInputGIS';
import { Draw } from 'ol/interaction';
import { addInteraction, clearDraw, mapperData } from '../MapService';
import { WKT } from 'ol/format';

// init maps features
const raster = new TileLayer({
    source: new OSM(),
});
const source = new VectorSource({ wrapX: false });
const vector = new VectorLayer({
    source: source,
});
const initView = {
    projection: 'EPSG:3857',
    center: [1388524.419, 2349898.577],
    zoom: 2,
};

type MapComponentProps = {
    type?: string;
    onChangeFeature?: any;
    wkt?: string;
}


export default function MapComponent(props: MapComponentProps) {

    const mapElement = useRef<HTMLDivElement | null>(null);
    const [map, setMap] = useState<Map>();
    const [drawInstance, setDrawInstance] = useState<Draw>();
    const [typeDraw, setTypeDraw] = useState<string>();
    const format = new WKT();

    const initialized = useRef(false);

    useEffect(() => {
        if (!initialized.current) {
            initialized.current = true
            initMap(mapElement);
            console.log('wkt: ', props.wkt);
        }
    }, []);

    function initMap(mapElement: React.MutableRefObject<HTMLDivElement | null>): Map | undefined {
        const map = new Map({
            layers: [raster, vector],
            view: new View(initView),
            controls: [],
        });
        map.setTarget(mapElement.current as any);
        mapperData(map, source);
        if (props.wkt && props.wkt !== '') {
            const feature = format.readFeature(props.wkt, {
                dataProjection: 'EPSG:3857',
                featureProjection: 'EPSG:3857',
                });
            const vector1 = new VectorLayer({
                source: new VectorSource({
                    features: [feature],
                }),
                style: MAP.MARKER.STYLE_DEFAULT
            });
            map?.addLayer(vector1);
        }
        setMap(map);
        return map;
    }

    const addInteractions = (type: any) => {
        if (type) {
            if (type != typeDraw) clearDraw(drawInstance);
            setDrawInstance(addInteraction(type, handleFeatureChange));
        } else {
            clearDraw(drawInstance);
        }
        // map?.removeLayer()
        setTypeDraw(type);
    }

    const handleFeatureChange = (wkt: string) => {
        props.onChangeFeature(wkt);
    };

    const onZoom = (zo: boolean) => {
        if (map) {
            let view = map.getView();
            let zoom = view.getZoom();
            if (zoom) view.setZoom(zo ? zoom + 1 : zoom - 1);
        }
    }

    return (
        <div className='map-wrapper'>
            <div className='map-container' ref={mapElement}>
                <div className="functions">
                    <MapDraw onDraw={(type: any) => addInteractions(type)} />
                    <MapInputGIS></MapInputGIS>
                    <MapInputData></MapInputData>
                    <MapZoom onZoom={(zoom: boolean) => onZoom(zoom)} />
                </div>
            </div>
        </div>
    )
}
