import React, { useEffect, useRef, useState } from "react";
import "./MapComponentV2.css"
import { fromLonLat } from "ol/proj";
import { Polygon, Geometry, Point } from "ol/geom";
import "ol/ol.css";
import {
    RMap,
    ROSM,
    RInteraction,
    RLayerVector,
    RStyle,
    RFeature,
    RControl,
    ROverlay,
    RLayerTile,
} from "rlayers";
import VectorSource from "ol/source/Vector";
import { OLFeatureClass } from "node_modules/rlayers/dist/context";
import { Feature, getUid } from "ol";
import { MAP } from "@src/constants/mapConstanst";
import { Alert, Button, Form, InputNumber, Modal, Space } from "antd";
import { DeleteOutlined } from '@ant-design/icons';
import GeoJSON from "ol/format/GeoJSON";
import ReactDOM from "react-dom";

type PointType = {
    longitude?: number,
    latitude?: number,
}

export default function MapComponentV2(
    { type = "Point", width = "100%", height = "50vh" }:
        { type?: string, width?: string, height?: string }): JSX.Element {

    const [vectorSource, setVectorSource] = useState<VectorSource<OLFeatureClass>>();
    const [activeDraw, setActiveDraw] = useState(false);
    const [currentLayer, setCurrentLayer] = useState("osm");

    const [pointFeature, setPointFeature] = useState<Feature>();
    const [polygonFeatures, setPolygonFeatures] = useState<Feature[]>([]);
    const [dataGis, setDataGis] = useState<Feature<Geometry>[]>([]);
    const [tempGis, setTempGis] = useState<Feature<Geometry>[]>([]);

    const [isOpenInputPoint, setIsOpenInputPoint] = useState(false);
    const [isOpenGis, setIsOpenGis] = useState(false);

    const handleMapChange = React.useCallback(
        (e: any) => {
            const source = e.target as VectorSource<OLFeatureClass>;
            setVectorSource(source);
        },
        []
    );

    const changeCurrentLayer = () => {
        if (currentLayer === "osm") {
            setCurrentLayer("aerial");
        } else {
            setCurrentLayer("osm");
        }
    }

    //Các function cho component map point
    const handleDrawPointStart = React.useCallback(() => {
        clearPoint();
    }, [vectorSource]);

    const handleDrawPointEnd = React.useCallback(
        (e: any) => {
            const point = e.feature;
            point.setStyle(MAP.MARKER.STYLE_DEFAULT);
        },
        []
    );

    const clearPoint = () => {
        setPointFeature(undefined);
        if (vectorSource) {
            const features = vectorSource.getFeatures();
            const filteredFeatures = features.filter(feature => feature.getGeometry()?.getType() !== 'Point');
            vectorSource.clear();
            vectorSource.addFeatures(filteredFeatures);
        }

        if (dataGis) {
            const filteredFeatures = dataGis.filter(feature => feature.getGeometry()?.getType() !== 'Point');
            setDataGis(filteredFeatures);
        }
    }

    const insertPoint = (values: any) => {
        clearPoint();
        const { longitude, latitude } = values;
        const point = new Feature({
            geometry: new Point(fromLonLat([longitude, latitude])),
        });
        point.setStyle(MAP.MARKER.STYLE_DEFAULT);

        if (vectorSource) {
            vectorSource.addFeature(point);
        }
        setPointFeature(point);
        setIsOpenInputPoint(false);
    }

    const showInputPoint = () => {
        setIsOpenInputPoint(!isOpenInputPoint);
    }

    const handleDrawPoint = () => {
        setActiveDraw(true);
    };

    //Các function cho component map polygon
    const handleDrawPolygonStart = React.useCallback((e: any) => {
        // const coordinates = e.feature.getGeometry().getCoordinates();
        // const polygon = new Polygon(coordinates);
        const polygon = e.feature;
        // setPolygonFeatures([...polygonFeatures, polygon])
    }, []);

    const handleDrawPolygonEnd = React.useCallback((e: any) => {
        // const coordinates = e.feature.getGeometry().getCoordinates();
        // const polygon = new Polygon(coordinates);
        const polygon = e.feature;
        setPolygonFeatures(polygonFeatures => [...polygonFeatures, polygon])
    }, []);

    const handleDeletePolygon = (polygon: Feature) => {
        const polygonUid = getUid(polygon);

        setPolygonFeatures((polygonFeatures) =>
            polygonFeatures.filter((feature) => getUid(feature) !== polygonUid)
        );

        if (vectorSource) {
            const features = vectorSource.getFeatures();
            vectorSource.clear();
            const filteredFeatures = features.filter(
                (feature) => getUid(feature) !== polygonUid
            );
            vectorSource.addFeatures(filteredFeatures);
        }
    };

    const calculateArea = (polygon: Feature) => {
        const geometry = polygon.getGeometry();

        if (geometry instanceof Polygon) {
            const area = geometry.getArea();
            const areaInSquareMeters = Math.round(area * 100) / 100;
            const formattedArea = areaInSquareMeters.toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            });
            return formattedArea;
        } else {
            return '0.00';
        }
    }

    const handleDrawPolygon = () => {
        setActiveDraw(true);
    };

    //xử lý file gis
    const fileInputRef = useRef<HTMLInputElement>(null);

    const showGis = () => {
        setIsOpenGis(!isOpenGis);
    }

    const handleUploadClick = () => {
        fileInputRef.current?.click();
    };

    const handleFileInputChange = (event: any) => {
        const file = event.target.files[0];
        const reader = new FileReader();

        reader.onload = (e: any) => {
            const content = e.target.result;
            const geoJsonData = JSON.parse(content);
            const features = new GeoJSON({ featureProjection: "EPSG:3857" }).readFeatures(geoJsonData);
            features.forEach((feature) => {
                if (feature.getGeometry()?.getType() === 'Point') {
                    feature.setStyle(MAP.MARKER.STYLE_DEFAULT);
                }
            });
            setTempGis(features);
        };

        reader.readAsText(file);
    };

    const handleUploadGis = () => {
        clearPoint();
        setDataGis(tempGis);
        setIsOpenGis(false);

    }

    return (
        <div className="general-v2">
            <RMap
                width={width}
                height={height}
                // noDefaultControls={true}
                initial={{ center: fromLonLat([103.9840, 10.2899]), zoom: 2 }}
            >
                {/* chuyển đổi giữa bản đồ thường và bản đồ không gian */}
                <ROSM
                    properties={{ label: "OpenStreetMap" }}
                    visible={currentLayer === "osm"} />
                <RLayerTile
                    properties={{ label: "MapBox" }}
                    url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"
                    attributions='<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a>'
                    visible={currentLayer === "aerial"}
                />


                {type === "Point" && (
                    <>
                        <RLayerVector onChange={handleMapChange}>
                            {activeDraw === true && (
                                <RInteraction.RDraw
                                    type={"Point"}
                                    style={MAP.MARKER.STYLE_DEFAULT}
                                    onDrawStart={handleDrawPointStart}
                                    onDrawEnd={handleDrawPointEnd}
                                />
                            )}
                        </RLayerVector>
                        <RLayerVector>
                            {pointFeature !== undefined && (
                                <RFeature feature={pointFeature}>

                                </RFeature>
                            )}
                        </RLayerVector>

                        <RControl.RCustom className="custom-control">
                            <div className="btn-custom-control">
                                <button onClick={handleDrawPoint}>
                                    <img src="/marker-black.png" alt="P" width="20" height="20" />
                                </button>
                                <button onClick={showInputPoint}>
                                    <img src="/coordinates.svg" alt="P" width="20" height="20" />
                                </button>
                                <button onClick={showGis}>
                                    <img src="/insert-gis.svg" alt="P" width="20" height="20" />
                                </button>
                            </div>
                        </RControl.RCustom>

                        <Modal title="Nhập thông tin" open={isOpenInputPoint} footer={null} width={300} onCancel={showInputPoint}>
                            <Alert message="Hệ thống sử dụng hệ tọa độ EPSG:4326, Bạn cần sử dụng chính xác hệ tọa độ để tương thích với hệ thống." type="warning" showIcon />
                            <Form
                                name="basic"
                                labelCol={{ span: 24 }}
                                wrapperCol={{ span: 24 }}
                                layout={"vertical"}
                                style={{ maxWidth: 300, marginTop: "20px" }}
                                onFinish={(values) => insertPoint(values)}
                            // onFinishFailed={onFinishFailed}
                            >
                                <Form.Item<PointType>
                                    label="Kinh độ:"
                                    name="longitude"
                                    rules={[{ required: true, message: 'Please input longitude!' }]}
                                >
                                    <InputNumber placeholder="Nhập kinh độ..." style={{ width: "100%" }} />
                                </Form.Item>

                                <Form.Item<PointType>
                                    label="Vĩ độ:"
                                    name="latitude"
                                    rules={[{ required: true, message: 'Please input longitude!' }]}
                                >
                                    <InputNumber placeholder="Nhập vĩ độ..." style={{ width: "100%" }} />
                                </Form.Item>
                                <Form.Item >
                                    <Space style={{ width: "100%", justifyContent: "end" }}>
                                        <Button htmlType="button" onClick={showInputPoint}>
                                            Hủy
                                        </Button>
                                        <Button type="primary" htmlType="submit">
                                            Tạo mới
                                        </Button>
                                    </Space>
                                </Form.Item>
                            </Form>
                        </Modal>
                    </>
                )}

                {type === "Polygon" && (
                    <>
                        <RLayerVector onChange={handleMapChange}>
                            {activeDraw === true && (
                                <RInteraction.RDraw
                                    type={"Polygon"}
                                    onDrawStart={handleDrawPolygonStart}
                                    onDrawEnd={handleDrawPolygonEnd}
                                />
                            )}
                        </RLayerVector>

                        <RLayerVector>
                            {polygonFeatures.map((polygon, index) => (
                                <RFeature feature={polygon} key={index}>
                                    <ROverlay className="example-overlay">
                                        <div>
                                            <span style={{ marginRight: "10px" }}>Diện tích: </span>
                                            <span>{calculateArea(polygon)} m2</span>
                                        </div>
                                        <button className="btn-delete" onClick={() => handleDeletePolygon(polygon)}>
                                            <DeleteOutlined /> Delete
                                        </button>
                                    </ROverlay>
                                </RFeature>
                            ))}
                        </RLayerVector>


                        <RControl.RCustom className="custom-control">
                            <div className="btn-custom-control">
                                <button onClick={handleDrawPolygon}>
                                    <img src="/draw-polygon.svg" alt="P" width="20" height="20" />
                                </button>
                                <button onClick={showGis}>
                                    <img src="/insert-gis.svg" alt="P" width="20" height="20" />
                                </button>
                            </div>
                        </RControl.RCustom>

                    </>
                )}
                <RLayerVector>
                    {dataGis.map((feature, index) => (
                        <RFeature key={index} feature={feature} />
                    ))}
                </RLayerVector>

                <Modal title="Nhập thông tin" open={isOpenGis} footer={null} width={300} onCancel={showGis}>
                    <Alert message="Hệ thống sử dụng hệ tọa độ EPSG:4326, Bạn cần sử dụng chính xác hệ tọa độ để tương thích với hệ thống." type="warning" showIcon />
                    <div className="upload-wrapper">
                        <div className="upload-file" onClick={handleUploadClick}>
                            <img src="/upload-gis.svg" alt="upload-gis" />
                            <div className="upload-file-desc">
                                Nhấn hoặc kéo thả để tải lên dữ liệu
                            </div>
                            <div className="upload-file-note">
                                Chỉ hỗ trợ tệp dữ liệu GIS
                            </div>
                        </div>
                        <input
                            ref={fileInputRef}
                            type="file"
                            accept=".json"
                            style={{ display: 'none' }}
                            onChange={handleFileInputChange}
                        />
                    </div>
                    <div className="bottom-group-btn">
                        <Space style={{ width: "100%", justifyContent: "end" }}>
                            <Button htmlType="button" onClick={showGis}>
                                Hủy
                            </Button>
                            <Button type="primary" htmlType="button" onClick={handleUploadGis}>
                                Tạo mới
                            </Button>
                        </Space>
                    </div>
                </Modal>


                <RControl.RCustom className="switcher-control">
                    <div className="btn-switcher-control">
                        <button onClick={changeCurrentLayer}>
                            <img src="/icon-map-aerial.svg" alt="A" />
                        </button>
                        <button onClick={changeCurrentLayer}>
                            <img src="/icon-map-osm.svg" alt="O" />
                        </button>
                    </div>
                </RControl.RCustom>
            </RMap>
        </div>
    );
}
