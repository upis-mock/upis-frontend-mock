import React from 'react'
import MapComponentV2 from '../MapComponentV2/MapComponentV2'
import { RFeature, RMap, ROverlay, RStyle } from 'rlayers'
import { fromLonLat } from 'ol/proj'

export default function GeneralMapV2() {
  return (
    <div>
      <MapComponentV2 type='Point' height='500px'></MapComponentV2>

      <RMap
        width="100%"
        height="500px"
        noDefaultControls={true}
        initial={{ center: fromLonLat([103.9840, 10.2899]), zoom: 2 }}
      >
        <RFeature >

        </RFeature>
        <ROverlay>

        </ROverlay>

        <RStyle.RStyle>

        </RStyle.RStyle>

      </RMap>
    </div>
  )
}
