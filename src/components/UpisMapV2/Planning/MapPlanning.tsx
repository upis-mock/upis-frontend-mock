import { Button, FloatButton } from 'antd'
import { fromLonLat } from 'ol/proj'
import React from 'react'
import { RControl, RMap, ROSM } from 'rlayers'

export default function MapPlanning(
    { width = "100%", height = "500px" }:
        { width?: string, height?: string }
) {

    return (
        <div className='map-planning'>
            <RMap
                width={width}
                height={height}
                noDefaultControls={true}
                initial={{ center: fromLonLat([103.9840, 10.2899]), zoom: 2 }}
            >
                <ROSM />
                <RControl.RCustom className="action-control">
                    <Button.Group>
                        <Button>A</Button>
                        <Button>B</Button>
                    </Button.Group>
                    <Button.Group>
                        <Button>A</Button>
                        <Button>B</Button>
                    </Button.Group>
                    <Button.Group>
                        <Button>A</Button>
                        <Button>B</Button>
                    </Button.Group>
                </RControl.RCustom>
                <RControl.RCustom className="fullscreen-control">
                    <Button>A</Button>
                </RControl.RCustom>
            </RMap>
        </div>
    )
}
