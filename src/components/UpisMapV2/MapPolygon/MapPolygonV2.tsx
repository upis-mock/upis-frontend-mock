import { MAP } from '@src/constants/mapConstanst';
import { dataMapPointAtom } from '@src/jotai/map';
import { jotaiStore } from '@src/jotai/store';
import { Button, Modal } from 'antd';
import { useAtom } from 'jotai';
import { Feature } from 'ol';
import { Point } from 'ol/geom';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import React, { useEffect, useState } from 'react'
import './MapPolygonV2.css';
import MapComponentV2 from '../MapComponentV2/MapComponentV2';

export default function MapPolygonV2() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [pointData, setPointData] = useAtom(dataMapPointAtom);

  const iconFeature = new Feature({
    geometry: new Point([pointData[0], pointData[1]]),
  });

  const iconStyle = MAP.MARKER.STYLE_DEFAULT;
  iconFeature.setStyle(iconStyle);

  const vectorSource = new VectorSource({
    features: [iconFeature],
  });

  const vectorLayer = new VectorLayer({
    source: vectorSource,
  });

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <div className='map-point-wrapper'>
      <Button type="primary" onClick={showModal}>
        Open Map
      </Button>
      <Modal title="UPIS MAP" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} width={1000}>
        <MapComponentV2 type='Polygon' height='500px'></MapComponentV2>
      </Modal>
      <div>
        <div>
          Data: {JSON.stringify(pointData)}
        </div>
      </div>
    </div>
  );
}
