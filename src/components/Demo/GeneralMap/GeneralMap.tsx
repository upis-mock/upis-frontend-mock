import React, { useEffect, useRef, useState } from 'react'
import MapComponent from '@src/components/UpisMap/MapComponent/MapComponent'
import MapZoom from '@src/components/UpisMap/ControllComponent/MapZoom/MapZoom'
import { Map, View } from "ol";
import TileLayer from 'ol/layer/Tile';
import { OSM } from 'ol/source';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import "./GeneralMap.scss";
import { GeoJSON } from 'ol/format';
import { MAP } from '@src/constants/mapConstanst';


// init maps features
const raster = new TileLayer({
  source: new OSM(),
});
const source = new VectorSource({ wrapX: false });
const vector = new VectorLayer({
  source: source,
});
const initView = {
  projection: 'EPSG:3857',
  center: [11559527.6577, 1264363.4906],
  zoom: 8,
};

const URL = 'http://192.168.1.201:18884';
const KEY_MAPSTUDIO = '1A0DC1C879CFF6DCE060007F01005579';
const TYPE_NAME = "SCP_HOME";
const SRS_NAME = "EPSG:3857";
const featureRequest = `<GetFeature xmlns="http://www.opengis.net/wfs" service="WFS" version="1.1.0" outputFormat="application/json" maxFeatures="30000" 
                          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                          xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd">
                          <Query typeName="${TYPE_NAME}" srsName="${SRS_NAME}" />
                        </GetFeature>`;

export default function GeneralMap() {
  const mapElement = useRef<HTMLDivElement | null>(null);
  const [map, setMap] = useState<Map>();
  const initialized = useRef(false);

  useEffect(() => {
    if (!initialized.current) {
      initialized.current = true
      const mapIntance = initMap(mapElement);
      getFeaturesWFS().then((geoJson) => {
        if (geoJson) {
          addGeoJson(mapIntance,geoJson);
        }
      });
    }
  }, []);

  function initMap(mapElement: React.MutableRefObject<HTMLDivElement | null>): Map | undefined {
    const map = new Map({
      layers: [raster, vector],
      view: new View(initView),
      controls: [],
    });
    map.setTarget(mapElement.current as any);
    setMap(map);
    return map;
  }

  async function getFeaturesWFS() {
    const apiMapStudio = `${URL}/mapstudio/wfs?KEY=${KEY_MAPSTUDIO}`;
    try {
      const response = await fetch(apiMapStudio, {
        method: 'POST',
        body: featureRequest,
      });
      const json = await response.json();
      return json;
    } catch (error) {
      return null;
    }
  }

  function addGeoJson(mapin: any, geoJson: any) {
    const vectorSource = new VectorSource({
      features: new GeoJSON().readFeatures(geoJson)
    });
    const vectorLayer = new VectorLayer({
      source: vectorSource,
      style: MAP.MARKER.STYLE_DEFAULT
    });
    mapin?.addLayer(vectorLayer);
  }

  const onZoom = (zo: boolean) => {
    if (map) {
      let view = map.getView();
      let zoom = view.getZoom();
      if (zoom) view.setZoom(zo ? zoom + 1 : zoom - 1);
    }
  }

  return (
    <>
      <div className='general-wrapper'>
        <div className='general-container' ref={mapElement}>
          <div className="functions">
            <MapZoom onZoom={(zoom: boolean) => onZoom(zoom)} />
          </div>
        </div>
      </div>
    </>
  )
}

