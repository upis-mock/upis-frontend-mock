import MapComponent from '@src/components/UpisMap/MapComponent/MapComponent';
import { Button, Flex, Form, Input, Modal, Space } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import Title from 'antd/es/typography/Title';
import { useReducer, useState } from 'react';

const initialState = { count: 0 };

export default function CreateIndustrial() {
  const [form] = Form.useForm();
  
  const onFinish = (values: any) => {
      createHome(values);
  };

  const onReset = () => {
      form.resetFields();
  };

  const onChangeFeature = (data?: any) => {
      form.setFieldsValue({ wkt: data });
  }

  const createHome = (homeRequest: any) => {
      fetch('http://localhost:29088/api/homes', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify(homeRequest),
      })
          .then(response => {
          })
          .catch(error => console.error(error));
  }


  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
      setIsModalOpen(true);
  };

  const handleOk = () => {
      setIsModalOpen(false);
  };

  const handleCancel = () => {
      setIsModalOpen(false);
  };

  return (
    <div className="flex justify-center h-screen my-16">
      <div className="bg-white shadow-md rounded-lg p-8 w-[900px]">
                <Flex className='w-full' justify="space-between">
                    <Title level={3}>Create Industrial Park</Title>
                    <Button href='/industrial'>Back</Button>
                </Flex>

                <Form form={form} onFinish={onFinish} layout='vertical' className='form-wrapper'>
                    <Form.Item label="Name:" name="name" required>
                        <Input placeholder='Enter name...'></Input>
                    </Form.Item>
                    <Form.Item label="Address:" name="address" required>
                        <Input placeholder='Enter address...'></Input>
                    </Form.Item>
                    <Form.Item label="Location:" >
                        <div onClick={showModal}>
                            <img src="/input-location.svg"></img>
                        </div>
                    </Form.Item>
                    <Form.Item label="WKT:" name="wkt">
                        <TextArea rows={5}></TextArea>
                    </Form.Item>
                    <Form.Item>
                        <Space align='end' style={{ justifyContent: 'flex-end', width: '100%' }}>
                            <Button htmlType="button" onClick={onReset}>
                                Reset
                            </Button>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Space>
                    </Form.Item>
                </Form>
                <Modal title="UPIS MAP" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} width={1000}>
                    <MapComponent onChangeFeature={(data: any) => onChangeFeature(data)}></MapComponent>
                </Modal>
            </div>
    </div>
  )
}
