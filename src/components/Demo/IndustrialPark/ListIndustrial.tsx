import { Button, Flex, Pagination, Table, TableProps } from 'antd';
import React from 'react';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import Title from 'antd/es/typography/Title';

interface IndustrialPark {
  id?: string;
  name?: string;
  address?: string;
  wkt?: string;
}

const columns: TableProps<IndustrialPark>['columns'] = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'WKT',
    dataIndex: 'wkt',
    key: 'wkt',
  },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    render: (_, record) => (
      <Flex gap="small">
          <Button type="primary" ghost icon={<EditOutlined />}/>
          <Button type="primary" danger ghost icon={<DeleteOutlined />}/>
      </Flex>
    ),
  }
];

const data: IndustrialPark[] = [
  {
    id: '1',
    name: 'John Brown',
    address: 'New York No. 1 Lake Park',
  },
  {
    id: '2',
    name: 'Jim Green',
    address: 'London No. 1 Lake Park',
  },
  {
    id: '3',
    name: 'Joe Black',
    address: 'Sydney No. 1 Lake Park',
  },
];


export default function ListIndustrial() {
  return (
    <div className="flex justify-center h-screen my-16">
      <div className="bg-white shadow-md rounded-lg p-8 w-[900px]">
          <Flex className='w-full' justify="space-between">
              <Title level={3}>List Industrail Park</Title>
              <Button type='primary' href='/industrial/create'>+ Create</Button>
          </Flex>
          <Table columns={columns} dataSource={data} rowKey="id" />
      </div>
    </div>
  )
}
