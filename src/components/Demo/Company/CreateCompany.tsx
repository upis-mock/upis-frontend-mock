import { useEffect, useState } from 'react'
import TextArea from 'antd/es/input/TextArea';
import { Button, Flex, Form, Input, Modal, Space } from 'antd';
import MapComponent from '@src/components/UpisMap/MapComponent/MapComponent';
import Title from 'antd/lib/typography/Title';
import CompanyService from '@src/api/CompanyService';
import { useNavigate, useParams } from 'react-router-dom';
import MapView from '@src/components/UpisMap/MapComponent/MapView';

export default function CreateCompany() {
    const { companyId } = useParams<{ companyId: string }>();
    const [form] = Form.useForm();
    const navigate = useNavigate();
    const [wkt, setWKT] = useState(null);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const onFinish = (values: any) => {
        if (companyId) {
            updateCompany(companyId, values);
        } else {
            createHome(values);
        }
    };

    const onReset = () => {
        form.resetFields();
    };

    const onChangeFeature = (data?: any) => {
        form.setFieldsValue({ wkt: data });
        setWKT(data);
        console.log('Linh test: ', data);
        
    }

    const createHome = (homeRequest: any) => {
        CompanyService.create(homeRequest).then(res => {
            navigate("/company");
        }).catch(error => console.error(error));
        // fetch('http://localhost:29088/api/homes', {
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/json',
        //     },
        //     body: JSON.stringify(homeRequest),
        // })
        //     .then(response => {
        //         console.log('====================================');
        //         console.log(response);
        //         console.log('====================================');
        //     })
        //     .catch(error => console.error(error));
    }

    const updateCompany = (companyId: string, request: any) => {
        CompanyService.update(companyId, request).then(res => {
            navigate("/company");
        })
    }

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const getInfor = () => {
        if (companyId) {
            CompanyService.getDetail(companyId).then((res: any) => {
                form.setFieldsValue(res);
                setWKT(res.wkt)
            });
        }
    }

    const showModalEdit = (isEdit: boolean) => {
        if (isEdit) {
            setIsModalOpen(true);
        }
    }

    useEffect(() => {
        getInfor();
    });

    return (
        <div className="flex justify-center h-screen my-16">
            <div className="bg-white shadow-md rounded-lg p-8 w-[900px]">
                <Flex className='w-full' justify="space-between">
                    <Title level={3}>Create Company</Title>
                    <Button href='/company'>Back</Button>
                </Flex>

                <Form form={form} onFinish={onFinish} layout='vertical' className='form-wrapper'>
                    <Form.Item label="Name:" name="name" required>
                        <Input placeholder='Enter name...'></Input>
                    </Form.Item>
                    <Form.Item label="Address:" name="address" required>
                        <Input placeholder='Enter address...'></Input>
                    </Form.Item>
                    <Form.Item label="Location:" >
                        {!wkt ? 
                            <div onClick={showModal}>
                                <img src="/input-location.svg"></img>
                            </div> 
                        :
                            <div className='map-view'>
                                <MapView wkt={wkt} showPopup={(isEdit: boolean) => showModalEdit(isEdit)}></MapView>
                            </div>
                        }
                    </Form.Item>
                    <Form.Item label="WKT:" name="wkt">
                        <TextArea rows={5} disabled></TextArea>
                    </Form.Item>
                    <Form.Item>
                        <Space align='end' style={{ justifyContent: 'flex-end', width: '100%' }}>
                            <Button htmlType="button" onClick={onReset}>
                                Reset
                            </Button>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Space>
                    </Form.Item>
                </Form>
                <Modal title="UPIS MAP" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} width={1000}>
                    <MapComponent wkt={!wkt ? '' : wkt} onChangeFeature={(data: any) => onChangeFeature(data)}></MapComponent>
                </Modal>
            </div>
        </div>
    )
}
