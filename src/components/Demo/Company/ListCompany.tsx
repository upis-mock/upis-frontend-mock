import { ColumnsType } from 'antd/es/table';
import { Button, Table } from 'antd/lib';
import { useEffect, useState } from 'react';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { Flex, Popconfirm, PopconfirmProps } from 'antd';
import { Typography } from 'antd';
import CompanyService from '@src/api/CompanyService';
import ICompany from '@src/interfaces/ICompany';

const { Title } = Typography;


interface Company {
    id?: string;
    name?: string;
    address?: string;
    wkt?: string;
}

const dataSource = [
    {
      id: '1',
      name: 'Mike',
      address: '10 Downing Street',
    },
    {
      id: '2',
      name: 'John',
      address: '10 Downing Street',
    },
  ];

export default function ListCompany() {
    const [data, setData] = useState<ICompany[]>();
    const [loading, setLoading] = useState(false);

    const handleOk = (companyId: any) => {
        CompanyService.delete(companyId).then(res => {
            loadData()
        });
    };
    
    const columns: ColumnsType<ICompany> = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Address',
            dataIndex: 'address',
            key: 'address',
        },
        {
            title: 'WKT',
            dataIndex: 'wkt',
            key: 'wkt',
        },
        {
            title: 'Action',
            render: (_, record) => (
                // <button onClick={() => handleDelete(record.id)}>Delete</button>
                <Flex gap="small">
                    <Button type="primary" href={`/company/update/${record.id}`} ghost icon={<EditOutlined />}/>
                    <Popconfirm
                        title="Delete the task"
                        description="Are you sure to delete?"
                        okText="Yes"
                        cancelText="No"
                        onConfirm={() => handleOk(record.id)}
                    >
                        <Button type="primary" danger ghost icon={<DeleteOutlined />}/>
                    </Popconfirm>
                </Flex>
            ),
            width: "10%",
            key: 'action',
        },
    
    ];

    const loadData = () => {
        CompanyService.getList().then((res: any) => {
            if (res) {                
                setData(res);
                setLoading(false);
            }
        }).catch(function(e) {
            setLoading(false);
        });
    }
    
    useEffect(() => {
        // fetchData();
        loadData();
    }, []);


    return (
        <div className="flex justify-center h-screen my-16">
            <div className="bg-white shadow-md rounded-lg p-8 w-[900px]">
                <Flex className='w-full' justify="space-between">
                    <Title level={3}>List Company</Title>
                    <Button type='primary' href='/company/create'>+ Create</Button>
                </Flex>
                <Table
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                    rowKey="id"
                    pagination={false}
                ></Table>
            </div>
        </div>
    )
}
