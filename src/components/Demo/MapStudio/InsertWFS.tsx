import { fromLonLat } from 'ol/proj'
import React from 'react'
import { RMap, ROSM } from 'rlayers'

export default function InsertWFS() {
    return (
        <div>
            <RMap
                width="100%"
                height="500px"
                noDefaultControls={false}
                initial={{ center: fromLonLat([103.9840, 10.2899]), zoom: 2 }}>
                <ROSM></ROSM>
            </RMap>
        </div>
    )
}
