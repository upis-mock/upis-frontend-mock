import { Feature, Map, View } from 'ol';
import GeoJSON from 'ol/format/GeoJSON';
import { Point } from 'ol/geom';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import { fromLonLat } from 'ol/proj'
import { OSM } from 'ol/source';
import VectorSource from 'ol/source/Vector';
import React, { useEffect, useRef, useState } from 'react'


// init maps features
const raster = new TileLayer({
    source: new OSM(),
});
const source = new VectorSource({ wrapX: false });
const vector = new VectorLayer({
    source: source,
});
const initView = {
    projection: 'EPSG:4326',
    center: [108.47403, 15.57364],
    zoom: 2,
};

const URL = 'http://192.168.1.201:18884';
const KEY_MAPSTUDIO = '199607FB29BD2368E060007F0100493E';
const TYPE_NAME = "SCP_STATION_POINT";
const SRS_NAME = "EPSG:4326";
const featureRequest = `<GetFeature xmlns="http://www.opengis.net/wfs" service="WFS" version="1.1.0" outputFormat="application/json" maxFeatures="30000" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd"><Query typeName="${TYPE_NAME}" srsName="${SRS_NAME}" /></GetFeature>`;

export default function ViewWFS() {

    const mapElement = useRef<HTMLDivElement | null>(null);
    const [map, setMap] = useState<Map>();
    const initialized = useRef(false);


    function initMap(mapElement: React.MutableRefObject<HTMLDivElement | null>): Map | undefined {
        const map = new Map({
            layers: [raster, vector],
            view: new View(initView),
            controls: [],
        });
        map.setTarget(mapElement.current as any);
        return map;
    }

    async function getFeaturesWFS() {
        const apiMapStudio = `${URL}/mapstudio/wfs?KEY=${KEY_MAPSTUDIO}`;
        try {
            const response = await fetch(apiMapStudio, {
                method: 'POST',
                body: featureRequest,
            });
            const json = await response.json();
            return json;
        } catch (error) {
            console.error(error);
            return null; // Handle API call failure
        }
    }

    function addGeoJson(mapin: any, geoJson: any) {
        console.log('====================================');
        console.log(geoJson);
        console.log('====================================');
        
        const vectorSource = new VectorSource({
            features: new GeoJSON().readFeatures(geoJson)
        });

        const vectorLayer = new VectorLayer({
            source: vectorSource,
        });

        if (mapin) {
            console.log('====================================');
            console.log("hello");
            console.log('====================================');
            mapin.addLayer(vectorLayer);
        }
    }

    useEffect(() => {
        if (!initialized.current) {
            initialized.current = true;
            const mapIntance = initMap(mapElement);
            setMap(mapIntance);
            getFeaturesWFS().then((geoJson) => {
                if (geoJson) {
                    addGeoJson(mapIntance, geoJson);
                }
            });
        }

    }, []);


    return (
        <div>
            <div ref={mapElement} className="map" style={{ width: "100%", height: "500px" }}></div>
        </div>
    )
}
