import React, { useEffect, useState } from 'react'
import './MapPoint.css';
import TextArea from 'antd/es/input/TextArea';
import { Button, Form, Input, InputNumber, Modal, Space } from 'antd';
import MapComponent from '@src/components/UpisMap/MapComponent/MapComponent';

export default function MapPoint() {

  const [form] = Form.useForm();

  const onFinish = (values: any) => {
    console.log(values);
    createHome(values);
  };

  const onReset = () => {
    form.resetFields();
  };

  const onChangeFeature = (data?: any) => {
    form.setFieldsValue({ wkt: data });
  }

  const createHome = (homeRequest: any) => {
    fetch('http://localhost:29088/api/homes',  {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(homeRequest),
    })
      .then(response => {
        console.log('====================================');
        console.log(response);
        console.log('====================================');
      })
      .catch(error => console.error(error));
  }


  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <div className='map-point-wrapper'>
      <div className='map-point-content'>
        <Form form={form} onFinish={onFinish} layout='vertical' className='form-wrapper'>
          <Form.Item label="Area:" name="area" required>
            <InputNumber placeholder='Enter area...' style={{width: "100%"}}></InputNumber>
          </Form.Item>
          <Form.Item label="Address:" name="address" required>
            <Input placeholder='Enter address...'></Input>
          </Form.Item>
          <Form.Item label="Location:" >
            <div onClick={showModal}>
              <img src="/input-location.svg"></img>
            </div>
          </Form.Item>
          <Form.Item label="Data feature:" name="wkt">
            <TextArea rows={5}></TextArea>
          </Form.Item>
          <Form.Item>
            <Space align='end' style={{ justifyContent: 'flex-end', width: '100%' }}>
              <Button htmlType="button" onClick={onReset}>
                Reset
              </Button>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Space>
          </Form.Item>
        </Form>
        <Modal title="UPIS MAP" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} width={1000}>
          <MapComponent onChangeFeature={(data: any) => onChangeFeature(data)}></MapComponent>
        </Modal>

      </div>

    </div>
  );
}
