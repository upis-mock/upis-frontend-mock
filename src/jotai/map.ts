import { atom } from "jotai";


//Variable for map point component
export const zoomMapPointAtom = atom<any>([]);
export const dataMapPointAtom = atom<any>([]);
export const dataMapPolygonAtom = atom<any>([]);
