import { createStore } from "jotai";

// https://jotai.org/docs/guides/using-store-outside-react
// https://jotai.org/docs/core/store
export const jotaiStore = createStore()

// const [value, setValue  ] = useAtom ( stateATom )

// jotaiStore.set(stateATom ,  ... )
// jotaiStore.get(stateATom )