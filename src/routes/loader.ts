// import { redirect } from "react-router-dom";
import { getAccessToken } from "@src/utils/token"
import { NAME_ROUTES } from "./nameRoutes";
import { redirect } from "react-router-dom";
import { fakeApiRoles } from "@src/fake/fake";
import handleRequest from "@src/utils/handleRequest";

export const redirectIfToken = () => {
    const accessToken = getAccessToken()

    if(accessToken){
        // throw redirect(`/`);
    }

    return ({
        accessToken
    })
};

export const authGuard = () => {
    const accessToken = getAccessToken()
};

const checkRoles = (userRoles: string[], expectedRoles: string[]): boolean => {
    return expectedRoles.some((role) => userRoles.includes(role));
  }

export const roleGuard = async( 
    expectedRoles: string[] = [], 
    redirectTo: string = '/') => {
    const [err, data] = await handleRequest(fakeApiRoles(['super_admin', 'admin'], 300));

    if(err){
        return redirect(redirectTo ?? `/`);
    };

    const expect = checkRoles(data, expectedRoles);
    console.log('user roles: ', data)
    if(!expect){
        return redirect("/test/index");
    }
   
    return {
        roles : data
    }
}