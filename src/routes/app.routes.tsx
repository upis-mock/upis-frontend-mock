// import ProtectedRoutes from "@src/layouts/ProtectedRoutes";
// import Root from "@src/layouts/Root";
import { authRoutes } from "@src/modules/auth/auth.routes";
import { baseRoutes } from "@src/modules/base/base.routes";
import HomePage from "@src/modules/home/pages/HomePage";
import { testRoutes } from "@src/modules/test/test.routes";
import { createBrowserRouter } from "react-router-dom";
import BasicLayout from "@src/layouts/basicLayout/BasicLayout";
import { roleGuard } from "./loader";
import { seminarRoutes } from "@src/modules/seminar/seminar.routes";
import GeneralMap from "@src/components/Demo/GeneralMap/GeneralMap";
import MapPointV2 from "@src/components/UpisMapV2/MapPoint/MapPointV2";
import MapPolygonV2 from "@src/components/UpisMapV2/MapPolygon/MapPolygonV2";
import ListIndustrial from "@src/components/Demo/IndustrialPark/ListIndustrial";
import CreateIndustrial from "@src/components/Demo/IndustrialPark/CreateIndustrial";
import ListCompany from "@src/components/Demo/Company/ListCompany";
import CreateCompany from "@src/components/Demo/Company/CreateCompany";

export const appRoutes = createBrowserRouter([
    ...testRoutes,
    ...authRoutes,
    {
      path: "/",
      // element: <Root/>,
      // element: <ProtectedRoutes/>,
      element: <BasicLayout/>,
      children: [
        {
          path: "home",
          element: <HomePage />,
          loader: () => roleGuard(['admin1', 'admin'],'/abc')
        },
        {
          path: "/",
          element: <HomePage />,
          // loader: () => roleGuard(['admin'])
        },
        {
          path: "/upis-map",
          element: <GeneralMap/>,
        },
        {
          path: "/industrial",
          element: <ListIndustrial/>,
        },
        {
          path: "/industrial/create",
          element: <CreateIndustrial/>,
        },
        {
          path: "/company",
          element: <ListCompany/>,
        },
        {
          path: "/company/create",
          element: <CreateCompany/>,
        },
        {
          path: "/company/update/:companyId",
          element: <CreateCompany/>,
        },
        {
          path: "/map-point-rlayers",
          element: <MapPointV2/>,
        },
        {
          path: "/map-polygon-rlayers",
          element: <MapPolygonV2/>,
        },
      ],
    },

    // seminar 
    ...seminarRoutes,
    // end seminar
    ...baseRoutes,
  ], { basename: "/" },);
