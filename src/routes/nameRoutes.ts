export const NAME_ROUTES = {
    home: "/",
    login: "/login",
    page404: "/404",
    page500: "/500",
    profile: "/profile",
  };
