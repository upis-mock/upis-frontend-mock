import wait from "@src/utils/wait"

export const fakeApiRoles = async ( data: any[] , ms: number = 200) =>{ 
    await wait(ms);
    return data
}