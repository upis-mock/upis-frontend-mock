import { atom } from "jotai";
import languages from "./languages";
import { Locale } from "antd/es/locale";

export const localeAtom = atom<Locale>(languages.vi);
