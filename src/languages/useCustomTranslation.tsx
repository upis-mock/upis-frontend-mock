import { useTranslation } from "react-i18next";
import languages from "./languages";
import { useSetAtom } from "jotai";
import { localeAtom } from "./state";
export default function useCustomTranslation() {
    const { i18n, ...rest } = useTranslation(); 
    const setLocale = useSetAtom(localeAtom);
    const changeLocale = (lang: 'en' | 'vi') =>{
        i18n.changeLanguage(lang);
        setLocale(languages[lang]);
    }
    return {changeLocale, ...rest}
}
