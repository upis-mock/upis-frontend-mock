import vi_VN from 'antd/locale/vi_VN'
import en_US from 'antd/locale/en_US'

const languages  = {
    vi: vi_VN,
    en: en_US
}

export default languages