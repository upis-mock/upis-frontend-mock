import i18n from 'i18next';
import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
import en from './en';
import vi from './vi';
i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next) // bind react-i18next to the instance
  .init({
    fallbackLng: 'en',
    interpolation: {
      // React already does escaping
      escapeValue: false,
    },
    lng: 'vi',
    resources: {
      en: {
        translation: en,
      },
      vi: {
        translation: vi,
      },
    },
  })

export default i18n