import ENV from "@src/constants/env";
import pkceChallenge from "pkce-challenge";
import qs from "qs";


export const buildAuthParams = async (code:string) => {
  const code_verifier = localStorage.getItem('code_verifier');
  const params = {
    redirect_uri:  window.location.origin + ENV.AUTH.REDIRECT_URL,
    code_verifier,
    grant_type: ENV.AUTH.GRANT_TYPE,
    code: code??''
  };
  return qs.stringify(params);
}

export const  authorizeUrl =async (baseUrl: string ): Promise<string>  => {
    const { code_challenge, code_verifier} = await pkceChallenge();
    localStorage.setItem('code_verifier', code_verifier)
    const params = {
      response_type: ENV.AUTH.RESPONSE_TYPE,
      client_id: ENV.AUTH.CLIENT_ID,
      redirect_uri:  window.location.origin + ENV.AUTH.REDIRECT_URL,
      code_challenge: code_challenge,
      code_challenge_method: ENV.AUTH.CODE_CHALLENGE_METHOD,
      code_verifier,
      grant_type: ENV.AUTH.GRANT_TYPE,
      state: window.location.href
    };
    // console.log('prompt:>>', prompt)
    const pathname = qs.stringify(params);
    console.log('pathname', pathname)
    return `${baseUrl}?${pathname}`;
}

  
export const login = async () =>{
    window.location.href = await authorizeUrl(`${ENV.AUTH.AUTH_URL}/authorize`);
}