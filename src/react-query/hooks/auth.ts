import { useMutation } from "@tanstack/react-query";
import authApi from "@src/api/authApi";

export const useLogoutMutation = (options = {}) => {
  return useMutation({
    mutationFn: authApi.logout,
    onSuccess: () => {
      console.log("logged out app!");
    },
    onError: () => {
      console.log("logout failed");
    },
    ...options,
  });
};
