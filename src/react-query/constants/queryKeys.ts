const queryKeys = {
    base:{
        getBase: 'base/getBase',
        getBaseByCode: 'base/getBaseByCode',
        getBaseByCodeAndLanguage: 'base/getBaseByCodeAndLanguage',
        getBaseByCodeAndLanguageAndVersion: 'base/getBaseByCodeAndLanguageAndVersion',
    },
    user: {
      getUserInfo: "user/get-user-info",
    },
  };
  
  export default queryKeys;
  