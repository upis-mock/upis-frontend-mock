export default interface IIndistrialPark 
{
    id: string;
    name: string;
    address: string;
    wkt: string;
}