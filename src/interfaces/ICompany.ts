export default interface ICompany 
{
    id?: string;
    name?: string;
    address?: string;
    wkt?: string;
}