const handleRequest = (promise: Promise<any>) => {
    return promise
      .then((data) => [undefined, data])
      .catch((err) => [err, undefined]);
  };
  
export default handleRequest;
  