import getTypeOf from "./getTypeOf";

export default function isObject(value: any): boolean {
  return getTypeOf(value) === 'Object';
}
