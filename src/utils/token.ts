import { ACCESS_TOKEN, REFRESH_ACCESS_TOKEN } from "@src/constants/app";
import Cookies from 'js-cookie';

export const getAccessToken = () => {
  return Cookies.get(ACCESS_TOKEN);
};

export const getRefreshToken = () =>{
  return Cookies.get(REFRESH_ACCESS_TOKEN);
}