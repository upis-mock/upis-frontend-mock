function objectToFormData(obj: any, rootName?: string | undefined, ignoreList: string[] | undefined = undefined): FormData {
    const formData = new FormData();
  
    function appendFormData(data: any, root?: string | undefined) {
      if (!ignore(root)) {
        root = root || '';
        if (data instanceof File) {
          formData.append(root, data);
        } else if (Array.isArray(data)) {
          for (let i = 0; i < data.length; i++) {
            appendFormData(data[i], `${root}[${i}]`);
          }
        } else if (typeof data === 'object' && data) {
          for (const key in data) {
            // eslint-disable-next-line no-prototype-builtins
            if (data.hasOwnProperty(key)) {
              if (root === '') {
                appendFormData(data[key], key);
              } else {
                appendFormData(data[key], `${root}.${key}`);
              }
            }
          }
        } else {
          if (data !== null && typeof data !== 'undefined') {
            formData.append(root, data);
          }
        }
      }
    }
  
    function ignore(root?: string | undefined) {
      return (
        Array.isArray(ignoreList) &&
        ignoreList.some((x) => {
          return x === root;
        })
      );
    }
  
    appendFormData(obj, rootName);
  
    return formData;
  }
  
  
  export default objectToFormData
  