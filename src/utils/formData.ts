import getTypeOf from "./getTypeOf";
import isObject from "./isObject";

export const containsFile = (body: any) :boolean  =>{
    if (!body || !isObject(body)) {
      return false; // Nếu body không tồn tại hoặc không phải là một object, trả về false
    }

    // Duyệt qua tất cả các trường của body
    for (const key in body) {
      // eslint-disable-next-line no-prototype-builtins
      if (body.hasOwnProperty(key)) {
        const value = body[key];
        // Kiểm tra nếu giá trị của trường là một File hoặc một instance của Blob (dạng dữ liệu tương tự file)
        if (
          value instanceof File ||
          (value instanceof Blob && 'name' in value && (typeof value?.name === 'string' || getTypeOf(value?.name) === 'String'))
        ) {
          return true; // Nếu tìm thấy bất kỳ trường nào là một File hoặc một Blob, trả về true
        }
      }
    }
    return false; // Nếu không tìm thấy trường nào là một File hoặc một Blob, trả về false
  }