/**
 * Returns the type of the given object.
 *
 * @example
 * getTypeOf('hello') // => "String"
 * getTypeOf([]) // => "Array"
 * getTypeOf({}) // => "Object"
 * getTypeOf(null) // => "Null"
 * getTypeOf(undefined) // => "Undefined"
 *
 * @param something - The object to get the type of.
 *
 * @returns The name of the type of the given object.
 */
export default function getTypeOf(something: any): string {
    const typeString = Object.prototype.toString.call(something).slice(8, -1);
    return typeString;
  }
  
  