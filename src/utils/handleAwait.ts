export const handleAwait = async (isRefreshing$: any, callback: any) => {
    const INCREMENT_TIME = 100;
    let waited = 0;
  
    await waitAwhile(isRefreshing$);
  
    async function waitAwhile(isRefreshing$: any) {
      const WAIT_LIMIT_TIME = 15000;
  
      if (waited == WAIT_LIMIT_TIME) return;
      waited += INCREMENT_TIME;
      if (isRefreshing$.getValue() === false) {
        callback();
      } else {
        setTimeout(waitAwhile, INCREMENT_TIME);
      }
    }
  };
  