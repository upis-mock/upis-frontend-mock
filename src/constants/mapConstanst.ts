import TileLayer from "ol/layer/Tile";
import { OSM, XYZ } from "ol/source";
import { Fill, Icon, Stroke, Style } from "ol/style";
import CircleStyle from "ol/style/Circle";


export const QUY_NHON_CENTER = [13.7830, 109.2197]
export const ZOOM_DEFAULT = 8;

export const MAP = {
    BASE_LAYER: {
        OSM_LAYER: new TileLayer({
            source: new OSM(),
        }),
        AERIAL_LAYER: new TileLayer({
            source: new XYZ({
                attributions: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> ',
                url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                maxZoom: 18,
            }),
        })
    },
    MARKER: {
        STYLE_DEFAULT: new Style({
            image: new Icon({
                src: '/marker.png',
                scale: 0.1,
                anchor: [0.5, 1],
            }),
        })
    },
    POLYGON: {
        STYLE_DEFAULT: new Style({
            fill: new Fill({
                color: 'rgba(255, 255, 255, 0.2)',
            }),
            stroke: new Stroke({
                color: 'rgba(0, 0, 0, 0.5)',
                lineDash: [10, 10],
                width: 2,
            }),
            image: new CircleStyle({
                radius: 5,
                stroke: new Stroke({
                    color: 'rgba(0, 0, 0, 0.7)',
                }),
                fill: new Fill({
                    color: 'rgba(255, 255, 255, 0.2)',
                }),
            }),
        })
    }

}