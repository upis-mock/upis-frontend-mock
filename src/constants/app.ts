export const ACCESS_TOKEN = "ACCESS_TOKEN";

export const REFRESH_ACCESS_TOKEN = "REFRESH_ACCESS_TOKEN";

export const CODE_VERIFIER = 'code_verifier'