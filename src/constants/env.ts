const ENV = {
    NODE_ENV: import.meta.env.VITE_NODE_ENV,
    API_URL: import.meta.env.VITE_API_URL,
    AUTH: {
        AUTH_URL : import.meta.env.VITE_AUTH_URL,
        RESPONSE_TYPE : import.meta.env.VITE_RESPONSE_TYPE,
        CLIENT_ID:  import.meta.env.VITE_CLIENT_ID,
        VITE_CLIENT_SECRET:  import.meta.env.VITE_CLIENT_SECRET,
        REDIRECT_URL: import.meta.env.VITE_REDIRECT_URL,
        CODE_CHALLENGE_METHOD: import.meta.env.VITE_CODE_CHALLENGE_METHOD,
        GRANT_TYPE: import.meta.env.VITE_GRANT_TYPE,
    }
}

export default ENV;