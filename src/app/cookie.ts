import { ACCESS_TOKEN } from '@src/constants/app';
import Cookies from 'js-cookie';

export function deleteAllCookies() {
  const cookies = document.cookie.split(";");

  for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i];
      const eqPos = cookie.indexOf("=");
      const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
  }
}

export const removeCookie = () => {
  Cookies.remove(ACCESS_TOKEN);
  localStorage.removeItem(ACCESS_TOKEN);
};

export const clearCookie = () => {
  // Get all the cookie keys
  // Cookies.remove(ACCESS_TOKEN);

  deleteAllCookies();
  // // Loop through the keys and remove each cookie
  // cookieKeys.forEach((key: string) => {
  //   cookies.remove(key);
  // });
};
