import { Outlet } from 'react-router-dom'
export default function TestLayout() {
  return (
    <div>
        <h1>
            Test layout
        </h1>
 
        <Outlet />
    </div>
  )
}

