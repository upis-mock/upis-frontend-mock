import React from 'react';

import dayjs from 'dayjs';

import { ConfigProvider } from 'antd';
import 'dayjs/locale/en';
import 'dayjs/locale/vi';
import { useAtomValue } from 'jotai';
import { localeAtom } from '@src/languages/state';
dayjs.locale('vi')

type AuthLayoutProps = {
    children: React.ReactElement<any, string | React.JSXElementConstructor<any>>
}
export default function ConfigLayout( {children} : AuthLayoutProps ) {
    const locale = useAtomValue(localeAtom);
    return (
        <ConfigProvider locale={locale}>
            {children}
        </ConfigProvider>
    )
}