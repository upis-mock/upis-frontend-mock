import { Layout } from 'antd'
import React from 'react'

const { Footer } = Layout;

export const HEIGHT_BASIC_FOOTER = 72
export default function BasicFooter() {
  return (
    <Footer className='text-center sticky bottom-0' style={{height: HEIGHT_BASIC_FOOTER}}>
        <div>Jungdo UIT</div>
    </Footer>
  )
}
