import React from 'react'
import { Layout } from 'antd';
import BasicHeader from '@src/layouts/basicLayout/BasicHeader';
import BasicFooter from '@src/layouts/basicLayout/BasicFooter';
import BasicSider from '@src/layouts/basicLayout/BasicSider';
import { Outlet } from 'react-router-dom';

const { Content } = Layout;

const layoutStyle = {
  borderRadius: 8,
};

export default function BasicLayout() {
  return (
  <Layout style={layoutStyle}>
    <BasicHeader/>
    <Layout className='flex flex-row'>
      <BasicSider/>
      
      <Content>
        <Outlet/>
      </Content>
    </Layout>
    <BasicFooter/>
  </Layout>
  )
}