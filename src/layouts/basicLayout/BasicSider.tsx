import { Layout } from 'antd';
import React, { useRef, useState } from 'react'
import { HEIGHT_BASIC_HEADER } from './BasicHeader';
import { HEIGHT_BASIC_FOOTER } from './BasicFooter';
import {
  AppstoreOutlined,
  ContainerOutlined,
  DesktopOutlined,
  MailOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  PieChartOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Button, Menu } from 'antd';
import { useNavigate } from 'react-router-dom';

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem('Openlayers', '/ol', <PieChartOutlined />, [
    getItem('UPIS Map ', '/upis-map'),
    getItem('Industrial Park ', '/industrial'),
    getItem('Company ', '/company'),
  ]),
  getItem('RLayers', '/rlayers', <ContainerOutlined />, [
    getItem('Map Point ', '/map-point-rlayers'),
    getItem('Map Polygon ', '/map-polygon-rlayers'),
  ]),
];


const { Sider } = Layout;

const siderStyle: React.CSSProperties = {
  textAlign: 'center',
  lineHeight: '120px',
  color: '#fff',
  backgroundColor: '#1677ff',
  position: 'sticky',
  top: HEIGHT_BASIC_HEADER,
  maxHeight: `calc(100vh - ${HEIGHT_BASIC_HEADER + HEIGHT_BASIC_FOOTER}px)`,
};

export default function BasicSider() {
  const [collapsed, setCollapsed] = useState(false);
  const navigate = useNavigate();

  function handleClickItem(item: any) {
    navigate(item.key);
  }

  return (
    <div style={{ width: 256, }}>
        <Menu
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          mode="inline"
          theme="dark"
          inlineCollapsed={collapsed}
          items={items}
          style={{ height: "100vh" }}
          onClick={handleClickItem}
        />
    </div>
  );
}
