import React from 'react'
import { Layout, theme } from 'antd';
const { Header } = Layout;

export const HEIGHT_BASIC_HEADER = 64;

export default function BasicHeader() {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  
  return (
    <Header className='sticky top-0' style={{height: HEIGHT_BASIC_HEADER, background: colorBgContainer}}>
      <div className='text-center'>UPIS MAP</div>
    </Header>
  )
}