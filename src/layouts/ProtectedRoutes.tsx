import React from "react";

import { Navigate, Outlet } from "react-router-dom";
import { getAccessToken } from "@src/utils/token";

const ProtectedRoutes = () => {
	const localStorageToken = getAccessToken();
	return localStorageToken ? <Outlet /> : <Navigate to="/auth/login"  replace />;
};

export default ProtectedRoutes;