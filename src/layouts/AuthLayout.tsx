import authApi from '@src/api/authApi';
import React, { useEffect } from 'react'
import { buildAuthParams } from '@src/services/auth.service';

type AuthLayoutProps = {
    children: React.ReactElement<any, string | React.JSXElementConstructor<any>>
}

export default function AuthLayout( {children} : AuthLayoutProps ) {
    const searchParams = new URL((document as any).location).searchParams;
    const code = searchParams.get('code'); 
    const state = searchParams.get('state'); 
  
    useEffect(()=>{
    if(!code || !state) return;
    const setup = async () => {
      const params = await buildAuthParams(code);
      console.log('params', params)
      // eslint-disable-next-line no-debugger
      // debugger;
      const response = await authApi.login(params);
      console.log('response', response)
      window.location.href = state;
    }

    setup();

    },[code, state])
  return (
    children
  )
}